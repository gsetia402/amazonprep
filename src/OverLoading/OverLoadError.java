package OverLoading;

public class OverLoadError {

    private static final int show(int a){
        System.out.println(a);
        return a;
    }

    private static final byte show(byte a){
        System.out.println(a);
        return a;
    }

    private static final String show(String a){
        System.out.println(a);
        return a;
    }

    public static void main(String[] args) {

        OverLoadError loadError = new OverLoadError();

        loadError.show(7);

        loadError.show("AAA");

        loadError.show('a');


        byte a = 25;
        loadError.show(a);


//        loadError.show(2.6);


    }




}
