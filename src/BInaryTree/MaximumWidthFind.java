package BInaryTree;

public class MaximumWidthFind {

    Node root;

    public static void main(String[] args) {
        MaximumWidthFind tree = new MaximumWidthFind();


        tree.root = new Node(1);
        tree.root.left = new Node(2);
        tree.root.right = new Node(3);
        tree.root.left.left = new Node(4);
        tree.root.left.right = new Node(5);
        tree.root.right.right = new Node(8);
        tree.root.right.right.left = new Node(6);
        tree.root.right.right.right = new Node(7);


        // We have to print maximum width of the tree

        // getMaxWidth(tree.root);

    }

    int getHeight(Node node){

        if (node ==null){
            return 0;
        } else {
            int lHeight = getHeight(node.left);
            int rHeight = getHeight(node.right);


            /*
            if (lHeight > rHeight){
                return lHeight+1;
            } else {
                return rHeight + 1;
            }
            */

            /*
            We can write the above code like this as well

             */


            return (lHeight > rHeight) ? (lHeight +1) : (rHeight + 1);


        }


    }

    static class Node {
        int key;
        Node left,right;

        public Node(int key) {
            this.key = key;
            left =right=null;
        }
    }


}


