package Hashing;

import java.util.HashSet;

public class HashSetExample {

    public static void main(String args[]) {
        //Creating HashSet and adding elements
        HashSet<String> set = new HashSet();
        set.add("One");
        set.add("Two");
        set.add("Three");
        set.add("Four");
        set.add("Five");
        set.add("One");
        System.out.println(set);

        set.remove("One");
        System.out.println("After remove one " + set);


        HashSet<String> set1 = new HashSet();
        set1.add("gaurav");
        set1.add("pankaj");
        set.addAll(set1);

        System.out.println("After adding full set" + set);

        set.removeAll(set1);

        System.out.println("After removing full set" + set);

        set.removeIf(str->str.contains("ee"));

        System.out.println("After removingif()" + set);

        set.clone();

        System.out.println("After clone" + set);







//            Iterator<String> i=set.iterator();
//            while(i.hasNext())
//            {
//                System.out.println(i.next());
//            }
    }
}
